import milestones from '~/pages/milestones/shared';
import initMilestonesShow from '~/pages/milestones/shared/init_milestones_show';

document.addEventListener('DOMContentLoaded', () => {
  initMilestonesShow();
  milestones();
});
